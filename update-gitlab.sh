#!/bin/sh
echo "Download new image..."
sudo docker pull gitlab/gitlab-ce:latest
echo "Cleaning up old registry images..."
sudo docker exec gitlab gitlab-ctl registry-garbage-collect > /dev/null
echo "Creating backup..."
sudo docker exec gitlab gitlab-rake gitlab:backup:create SKIP=artifacts,registry
echo "Apply updates..."
sudo docker-compose up -d
